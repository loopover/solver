#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <vector>
#include "./board.hpp"

using std::vector;

int main(int argc, char const *argv[]) {
    Board<u_int8_t, 5, 5> board;

    std::vector<Move> moves = {
        Move { Axis::Row, 0, 2 },
        Move { Axis::Col, 1, 1 },
        Move { Axis::Row, 2, 3 }
    };

    for (auto move: moves) { board.move(move); }
    board.print();

    return 0;
}
