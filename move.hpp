#include <array>

enum Axis {
    Row = 1,
    Col
};

struct Move {
    Axis axis;
    int index;
    int n;
};
