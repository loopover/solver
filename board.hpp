#include <algorithm>
#include <array>
#include "./move.hpp"

template<typename T, u_int8_t cols, u_int8_t rows>
struct Board {
    T grid[rows][cols];

    Board() {
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                grid[r][c] = r * cols + c;
            }
        }
    }

    bool is_solved() {
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                if (grid[r][c] != r * cols + c) { return false; }
            }
        }
        return true;
    }

    void move_row(int index, int n) {
        T row[cols]; std::copy(grid[index], std::end(grid[index]), row);
        for (int i = 0; i < cols; i++) {
            grid[index][i] = row[(i - n + cols) % cols];
        }
    }

    void move_col(int index, int n) {
        T col[rows]; for (int r = 0; r < rows; r++) { col[r] = grid[r][index]; }
        for (int i = 0; i < rows; i++) {
            grid[i][index] = col[(i - n + rows) % rows];
        }
    }

    void move(Move move) {
        if (move.axis == Col) { move_col(move.index, move.n); }
        else { move_row(move.index, move.n); }
    }

    void move_reverse(Move move) {
        if (move.axis == Col) { move_col(move.index, -move.n); }
        else { move_row(move.index, -move.n); }
    }

    void print() {
        for (int row = 0; row < rows; row++) {
            printf("\n");
            for (int col = 0; col < cols; col++) {
                printf("%*d", 5, grid[row][col]);
            }
            printf("\n");
        }
        printf("\n");
    }
};
